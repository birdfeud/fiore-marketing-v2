// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Earlybird Starter',
  siteUrl: 'https://earlybird-starter.netlify.app',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        baseDir: './content',
        path: '**/*.md',
        typeName: 'MarkdownPage',
      }
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
      }
    }
  ]
}
